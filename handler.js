'use strict';
import fetchAndHydrate from 'trading-feature-creator';
import {fetchDateBased} from 'quote-fetching';
import {details} from 'namer';

module.exports.fetchFeatures = (event, context, cb) => {
  const {query: {ticker, id, klass, purpose}} = event;
  if (!ticker) return cb('ticker param is needed');
  if (!id) return cb('id param is needed');
  const period = klass === 'daily' ? '3y' : '14d';
  const namer = details(id, 'stocks');

  // if purpose is 'forecast' it will return only one quote.
  
  
  fetchAndHydrate(ticker, period, purpose, namer, {empty: true}).then((res) => {
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ data: res })
    };
    cb(null, response);
  })
  .catch((err) => {
    console.info('is there an error here', err);
    cb(err);
  });
};



